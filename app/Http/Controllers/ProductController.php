<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    public function index()
    {
        return \Illuminate\Support\Facades\Response::view('products', [
            'products' => Product::all()
        ]);
    }

    public function create()
    {
        return new Response('You can create a new product');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return \Illuminate\Support\Facades\Response::view('pview', [
            'product' => $product,
            'message' => 'You can view this product'
        ]);
    }

    public function edit($id)
    {
        try {
            $product = Product::find($id);
            $this->authorize('update', $product);
        } catch (AuthorizationException $exception) {
            return redirect('/products/view/' . $id);
        }

        return \Illuminate\Support\Facades\Response::view('peditdelete', [
            'message' => 'You can edit this product'
        ]);
    }

    public function destroy($id)
    {
        try {
            $product = Product::find($id);
            $this->authorize('delete', $product);
        } catch (AuthorizationException $exception) {
            return redirect('/products/view/' . $id);
        }
//        $result = Product::destroy($id);
        return \Illuminate\Support\Facades\Response::view('peditdelete', [
            'message' => 'You can delete this product'
        ]);
    }

    public function __construct()
    {
        $this->middleware('auth');
    }
}
