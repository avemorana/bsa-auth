<?php

namespace App\Http\Controllers;

use App\Entity\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GithubAuthController extends Controller
{
    protected $redirectPath = '/products';

    public function redirectToProvider(Request $request)
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleProviderCallback()
    {
        $socialUser = Socialite::driver('github')->user();

        $user = User::where(['email' => $socialUser->email])->first();

//        dd($socialUser);

        if (is_null($user)){
            $user = User::create([
                'name' => $socialUser->user['login'],
                'email' => $socialUser->email,
            ]);
        }

        Auth::login($user);

        return redirect($this->redirectPath);
    }
}
