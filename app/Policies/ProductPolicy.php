<?php

namespace App\Policies;

use App\Entity\User;
use App\Entity\Product;
use Illuminate\Auth\Access\HandlesAuthorization;
use phpDocumentor\Reflection\Types\Boolean;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function update(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $user->is_admin;
    }

    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\Entity\User  $user
     * @param  \App\Entity\Product  $product
     * @return mixed
     */
    public function delete(User $user, Product $product)
    {
        return $this->isUsersProduct($user, $product) || $user->is_admin;
    }

    private function isUsersProduct(User $user, Product $product)
    {
        return $product->user_id === $user->id;
    }
}
