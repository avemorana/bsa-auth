<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('marketplace');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products', 'ProductController@index');
Route::get('/products/view/{id}', 'ProductController@show');
Route::get('/products/edit/{id}', 'ProductController@edit');
Route::get('/products/delete/{id}', 'ProductController@destroy');
Route::get('/products/create', 'ProductController@create');

Route::get('/auth/github', 'GithubAuthController@redirectToProvider');
Route::get('/auth/github/callback', 'GithubAuthController@handleProviderCallback');