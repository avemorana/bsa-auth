@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="/products">All</a>
        @can('update', $product)
            <a href="/products/edit/{{$product->id}}">Edit</a>
        @endcan
        @can('delete', $product)
            <a href="/products/delete/{{$product->id}}">Delete</a>
        @endcan
        <div class="row justify-content-center">
            {{$message}}
            <br>
            {{$product}}
        </div>
    </div>
@endsection
