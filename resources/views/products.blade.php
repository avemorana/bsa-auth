@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <ul>
                @foreach($products as $product)
                    <li>
                        <a href="/products/view/{{$product->id}}">
                            {{$product->name}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
